package com.example.hw10;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class GamesFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_games, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView games = view.findViewById(R.id.gamesImageView);
        games.setImageResource(R.drawable.ic_games);
        TextView nameTextView = view.findViewById(R.id.nameTextView);
        TextView phoneTv = view.findViewById(R.id.phoneNumberTextView);
        phoneTv.setText("87023660613");
    }
}
